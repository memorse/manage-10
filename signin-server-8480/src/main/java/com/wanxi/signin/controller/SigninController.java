package com.wanxi.signin.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.wanxi.manage.common.CommonResult;
import com.wanxi.manage.pojo.Signin;
import com.wanxi.manage.util.Util;
import com.wanxi.signin.service.SigninService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
public class SigninController {

    @Autowired
    private SigninService signinService;

    @GetMapping("/sign")
    public CommonResult getUsers(String search){

        Map<String,Object> map= JSONObject.parseObject(search,Map.class);

        int currentPage = map.get("currentPage").toString()==null||map.get("currentPage").toString().equals("")?1:Integer.valueOf(map.get("currentPage").toString());
        
        Integer pageSize = map.get("pageSize").toString()==null||map.get("pageSize").toString().equals("")?10:Integer.valueOf(map.get("pageSize").toString());
        
        int startPage=(currentPage-1)*pageSize;
        
        map.put("currentPage",startPage);
        map.put("pageSize",pageSize);

        Page page = new Page(currentPage, pageSize);
        signinService.getSignins(page,map);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("list",page.getRecords());
        int pageCount = Util.getPageCount((int) page.getTotal(), pageSize);
        JSONObject jsonObject1 = Util.packageJSON(jsonObject, pageCount, pageSize, currentPage);
        return CommonResult.success(jsonObject1);
    }

    @GetMapping("/sign/{id}")
    public CommonResult getUserById(@PathVariable("id") String id){
        return CommonResult.success(signinService.getSiginByid(id));
    }

    @PutMapping("/sign")
    public CommonResult addUser(@RequestBody List<Signin> signs){
        String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        int isSigned = signinService.isSignedToday(currentDate);
        JSONObject jsonObject = new JSONObject();
        if(isSigned>0){
            jsonObject.put("status",200);
            jsonObject.put("result",-1);
            jsonObject.put("message","今天已经点过到了");
            return CommonResult.success(jsonObject);
        }
        int result=signinService.addSigns(signs);
        jsonObject.put("status",200);
        jsonObject.put("result",result);
        return CommonResult.success(jsonObject);
    }

    @PostMapping("/sign")
    public CommonResult updateUser(@RequestBody Signin signin){
        int i = signinService.updateSignin(signin);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status",200);
        jsonObject.put("result",i);

        return CommonResult.success(jsonObject);
    }

    @DeleteMapping("/sign")
    public CommonResult deleteUser(@RequestBody List<Integer> ids){
        int i = signinService.deleteSignin(ids);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("result",i);
        jsonObject.put("status",200);
        return CommonResult.success(jsonObject);
    }

}
