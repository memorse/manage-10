package com.wanxi.role.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.RolePermissionRalation;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface RolePermissionRelationMapper extends BaseMapper<RolePermissionRalation> {
    List<RolePermissionRalation> getPermissions(@Param("Search") Map<String, Object> map);
    IPage<RolePermissionRalation> getPermissions(Page page, @Param("Search") Map<String, Object> map);
    List<RolePermissionRalation> getRolePermissionByRoleCode(String roleCode);
    List<RolePermissionRalation> getRolePermissionByPerCode(String perCode);
    RolePermissionRalation getRolePermissionById(Integer id);
    int deletePermission(Integer id);
    int addPermission(RolePermissionRalation permission);
    int updatePermission(RolePermissionRalation permission);
    int isExsists(@Param("roleCode") String roleCode, @Param("perCode") String perCode);
}
