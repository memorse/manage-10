package com.wanxi.role.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.common.CommonResult;
import com.wanxi.manage.pojo.Role;
import com.wanxi.manage.util.Util;
import com.wanxi.role.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@CrossOrigin
@RestController
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping("/role")
    public CommonResult getUsers(String search){

        Map<String,Object> map= JSONObject.parseObject(search,Map.class);

        int currentPage = map.get("currentPage").toString()==null||map.get("currentPage").toString().equals("")?1:Integer.valueOf(map.get("currentPage").toString());
        
        Integer pageSize = map.get("pageSize").toString()==null||map.get("pageSize").toString().equals("")?10:Integer.valueOf(map.get("pageSize").toString());
        
        int startPage=(currentPage-1)*pageSize;
        
        map.put("currentPage",startPage);
        map.put("pageSize",pageSize);

        Page page = new Page(currentPage, pageSize);
        roleService.getRoles(page,map);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("list",page.getRecords());
        int pageCount = Util.getPageCount((int) page.getTotal(), pageSize);
        JSONObject jsonObject1 = Util.packageJSON(jsonObject, pageCount, pageSize, currentPage);
        return CommonResult.success(jsonObject1);
    }

    @GetMapping("/role/{id}")
    public CommonResult getUserById(@PathVariable("id") int id){
        return CommonResult.success(roleService.getRoleById(id));
    }

    @PutMapping("/role")
    public CommonResult addUser(@RequestBody Role role){
        int i = roleService.addRole(role);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status",200);
        jsonObject.put("result",i);
        return CommonResult.success(jsonObject);
    }

    @PostMapping("/role")
    public CommonResult updateUser(@RequestBody Role role){
        int i = roleService.updateRole(role);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status",200);
        jsonObject.put("result",i);

        return CommonResult.success(jsonObject);
    }

    @DeleteMapping("/role/{id}")
    public CommonResult deleteUser(@PathVariable("id") Integer id){
        int i = roleService.deleteRole(id);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("result",i);
        jsonObject.put("status",200);
        return CommonResult.success(jsonObject);
    }

}
