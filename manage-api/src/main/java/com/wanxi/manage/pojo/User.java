package com.wanxi.manage.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String code;
    private String name;
    private String gender;
    private Integer age;
    private String degree;
    private String graduateSchool;
    private String nativeAddress;
    private String address;
    private String phone;
    private String emergencyPhone;
    private String password;
    private Team groupCode;
    private Date updateTime;
    private Date createTime;
    private Integer enable;
    List<Permission> permissions;
}
