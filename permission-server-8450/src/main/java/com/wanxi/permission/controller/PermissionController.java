package com.wanxi.permission.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.common.CommonResult;
import com.wanxi.manage.pojo.Permission;
import com.wanxi.manage.util.Util;
import com.wanxi.permission.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class PermissionController {

    @Autowired
    private PermissionService service;

    @GetMapping("/permissions")
    public CommonResult getPermissions(@RequestParam Map map){
        int currentPage = map.get("currentPage")==null||map.get("currentPage").toString().equals("")?1:Integer.valueOf(map.get("currentPage").toString());
        Integer pageSize = map.get("pageSize")==null||map.get("pageSize").toString().equals("")?10:Integer.valueOf(map.get("pageSize").toString());
        int startPage=(currentPage-1)*pageSize;
        map.put("currentPage",startPage);
        map.put("pageSize",pageSize);
        map.put("key",map.get("key")==null?"":map.get("key"));
        if(map.get("endDate")==null)
            map.put("endDate","");
        if(map.get("startDate")==null)
            map.put("startDate","");
        Page page = new Page(currentPage, pageSize);
        service.getPermissions(page,map);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("list",page.getRecords());
        int pageCount = Util.getPageCount((int) page.getTotal(), pageSize);
        JSONObject jsonObject1 = Util.packageJSON(jsonObject, pageCount, pageSize, currentPage);
        return CommonResult.success(jsonObject1);
    }

    @GetMapping("/permission/{userCode}")
    public CommonResult getPermissionByUserCode(@PathVariable("userCode") String userCode){
        List<Permission> permissions = service.getPermissionByUserCode(userCode);
        if (permissions!=null)
            return CommonResult.success(permissions);
        else
            return CommonResult.failed("服务器忙，请稍后重试!");
    }
    @GetMapping("/permissionId/{id}")
    @PreAuthorize("hasAnyAuthority('query_all','manage_permission')")
    public CommonResult getPermissionById(@PathVariable("id") int id){
        Permission permission = service.getPermissionById(id);
        if (permission!=null)
            return CommonResult.success(permission);
        else
            return CommonResult.failed("服务器忙，请稍后重试!");
    }
    @DeleteMapping("/permission/{id}")
    @PreAuthorize("hasAuthority('manage_permission')")
    public CommonResult delPermissionById(@PathVariable("id") int id){
        int i  = service.deletePermission(id);

            return CommonResult.success(i);
    }
    @PostMapping("/permission")
    public CommonResult updatePermissionById(@RequestBody Permission permission){
        int i  = service.updatePermission(permission);
        return CommonResult.success(i);
    }

    @PutMapping("/permission")
    public CommonResult addPermissionById(@RequestBody Permission permission){
        int i  = service.addPermission(permission);
        return CommonResult.success(i);
    }
}
