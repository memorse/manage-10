package com.wanxi.manage.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserRoleShip implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private User userCode;
    private Role roleCode;
    private Date updateTime;
    private Date createTime;
    private Integer enable;
    public Integer getId() {
        return id;
    }

}
