package com.wanxi.userver.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.Team;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface TeamMapper extends BaseMapper<Team> {
    List<Team> getTeams(@Param("Search") Map<String, Object> map);
    IPage<Team> getTeams(Page page, @Param("Search") Map<String, Object> map);
    List<Team> getUsefulTeams();
    Team getTeamById(int id);
    Team getTeamByCode(String code);
    int updateTeam(Team team);
    int deleteTeam(String id);
    int addTeam(Team team);
}
