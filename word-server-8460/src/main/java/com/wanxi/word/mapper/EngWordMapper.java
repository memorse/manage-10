package com.wanxi.word.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.EngWord;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface EngWordMapper extends BaseMapper<EngWord> {
    List<EngWord> getWords(@Param("Search") Map map);
    IPage<EngWord> getWords(Page page, @Param("Search") Map map);
    int addWord(EngWord engWord);
    int updateWord(EngWord engWord);
    int deleteWord(Integer id);
}
