package com.wanxi.zull.config;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import com.wanxi.zull.utils.JwtTokenUtil;
import io.jsonwebtoken.ExpiredJwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashSet;
import java.util.List;

/**
 * JWT登录授权过滤器
 */
@Component
public class JwtAuthenticationTokenFilter extends ZuulFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(JwtAuthenticationTokenFilter.class);

    @Value("#{'${pathlist}'.split(',')}")
    private List<String> pathlist;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Value("${jwt.tokenHeader}")//Authorization
    private String tokenHeader;

    @Value("${jwt.tokenHead}")
    private String tokenHead;//bearer

    public boolean shouldFilter() {
        return true;
    }

    public Object run() throws ZuulException {
        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletResponse response = requestContext.getResponse();
        response.setContentType("text/html;charset=UTF-8");
        HttpServletRequest request = requestContext.getRequest();
        String uri = request.getRequestURI();
        //那些路径可以直接放行
        boolean a = pathlist.stream().anyMatch(path -> uri.contains(path));
        if (1==1) {
            requestContext.addZuulRequestHeader("token",request.getHeader("Authorization"));
            return null;
        }
        //从 header  中获取 Authorization
        String authHeader = request.getHeader(this.tokenHeader);
        // 判断 authHeader  不为空  并且以 bearer 开头
        if (authHeader != null) {
                boolean b1 = StringUtils.startsWithIgnoreCase(authHeader, this.tokenHead);
                if (b1) {
                    //截取 bearer 后面的字符串  并且 两端去空格（获取token）
                    String authToken = authHeader.substring(this.tokenHead.length()).trim();// The part after "Bearer "

                    String username = jwtTokenUtil.getUserNameFromToken(authToken);
                    try {
                        boolean flag=false;
                        HashSet<String> permission = jwtTokenUtil.getPermissionFromToken(authToken);
                        for (String s : permission) {
                            if (s.equals(uri)){
                                flag=true;
                                return null;
                            }
                        }
                        if(!flag){
                            requestContext.setResponseBody("您没有访问权限！");
                            return null;
                        }

                    } catch (Exception e) {
                        // 处理token过期
                        if (e instanceof ExpiredJwtException) {
                            requestContext.setResponseBody("token 过期");
                            return null;
                        }
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        public String filterType () {
            return "pre";
        }

        public int filterOrder () {
            return 0;
        }
    }

