package com.wanxi.manage.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class Team  implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String teamCode;
    private String teamName;
    private User teamLeader;
    private User teamLeaderCode;
    private Integer teamCrew;
    private Date updateTime;
    private Date createTime;
    private Integer enable;
}
