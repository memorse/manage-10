package com.wanxi.daily;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@MapperScan("com.wanxi.daily.mapper")
public class DailyServerApp {
    public static void main(String[] args) {
        SpringApplication.run(DailyServerApp.class,args);
    }
}
