package com.wanxi.word.service;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.WordCollection;

import java.util.List;
import java.util.Map;

public interface CollectionService extends BaseMapper<WordCollection>{
    List<WordCollection> getCollections(Map map);
    int deleteCollection(WordCollection collection);
    int addCollection(WordCollection collection);
    Integer isExsist(WordCollection collection);

    IPage<WordCollection> getCollections(Page page, Map map);
}
