package com.wanxi.team.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.Team;
import com.wanxi.team.mapper.TeamMapper;
import com.wanxi.team.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class TeamServiceImpl implements TeamService {

    @Autowired
    private TeamMapper teamMapper;
    @Override
    public List<Team> getTeams(Map<String, Object> map) {
        return teamMapper.getTeams(map);
    }

    @Override
    public IPage<Team> getTeams(Page page, Map<String, Object> map) {
        return teamMapper.getTeams(page,map);
    }

    @Override
    public List<Team> getUsefulTeams() {
        return teamMapper.getUsefulTeams();
    }

    @Override
    public Team getTeamById(int id) {
        return teamMapper.getTeamById(id);
    }

    @Override
    public Team getTeamByCode(String code) {
        return teamMapper.getTeamByCode(code);
    }

    @Override
    public int updateTeam(Team team) {
        return teamMapper.updateTeam(team);
    }

    @Override
    public int deleteTeam(String id) {
        return teamMapper.deleteTeam(id);
    }

    @Override
    public int addTeam(Team team) {
        return teamMapper.addTeam(team);
    }
}
