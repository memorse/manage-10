package com.wanxi.manage.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DailyReport implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String name;
    private String teacher;
    private String coatch;
    private String degree;
    private String gain;
    private String deficiency;
    private Date dateTime;
    private Date createTime;
    private Integer enable;
    private String userCode;
}
