package com.wanxi.role.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.RolePermissionRalation;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface RolePermissionRalationService {
    List<RolePermissionRalation> getPermissions(@Param("Search") Map<String, Object> map);
    IPage<RolePermissionRalation> getPermissions(Page page, @Param("Search") Map<String, Object> map);
    List<RolePermissionRalation> getRolePermissionByRoleCode(String roleCode);
    List<RolePermissionRalation> getRolePermissionByPerCode(String perCode);
    RolePermissionRalation getRolePermissionById(Integer id);
    int deletePermission(Integer id);
    int addPermission(List<RolePermissionRalation> permissions);
    int updatePermission(List<RolePermissionRalation> permissions);
    int isExsists(String roleCode, String perCode);
}
