package com.wanxi.signin.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.Signin;
import com.wanxi.signin.mapper.SigninMapper;
import com.wanxi.signin.service.SigninService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class SigninServiceImpl implements SigninService {

    @Autowired
    private SigninMapper mapper;
    @Override
    public List<Signin> getSignins(Map<String, Object> map) {
        return mapper.getSignins(map);
    }

    @Override
    public IPage<Signin> getSignins(Page page, Map<String, Object> map) {
        return mapper.getSignins(page,map);
    }

    @Override
    public Signin getSiginByid(String userCode) {
        return mapper.getSiginByid(userCode);
    }

    @Override
    public int updateSignin(Signin signin) {
        return mapper.updateSignin(signin);
    }

    @Override
    public int  deleteSignin(List<Integer> ids) {
        int result=0;
        for (Integer id : ids) {
            int i = mapper.deleteSignin(id);
            if(i>0)
                result=1;
            else
                result=0;
        }
        return result;
    }

    @Override
    public   int isSignedToday(String date) {
        return mapper.isSignedToday(date);
    }

    @Override
    public int addSigns(List<Signin> signins){
        int result=0;
        for (Signin signin : signins) {
            int i = mapper.addSignin(signin);
            if(i>0)
                result=1;
            else
                result=0;
        }
        return result;
    }
}
