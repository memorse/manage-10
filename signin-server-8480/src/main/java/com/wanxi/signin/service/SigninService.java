package com.wanxi.signin.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.Signin;

import java.util.List;
import java.util.Map;

public interface SigninService {
    List<Signin> getSignins(Map<String, Object> map);
    IPage<Signin> getSignins(Page page, Map<String, Object> map);
    Signin getSiginByid(String userCode);
    int updateSignin(Signin signin);
    int deleteSignin(List<Integer> ids);
    int isSignedToday(String date);
    int addSigns(List<Signin> signins);
}
