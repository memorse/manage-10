package com.wanxi.userver.controller;

import com.alibaba.fastjson.JSONObject;
import com.wanxi.manage.util.ImageVerificationCode;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
public class UtilController {


    @GetMapping("/getVerifyImage")
    public void getVerfiyImg(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        ImageVerificationCode ivc = new ImageVerificationCode();     //用我们的验证码类，生成验证码类对象
        BufferedImage image = ivc.getImage();  //获取验证码
        String text = ivc.getText();
        req.getSession().setAttribute("code", text); //将验证码的文本存在session中

        System.out.println(req.getSession().getAttribute("code"));
        ivc.output(image, resp.getOutputStream());//将验证码图片响应给客户端
    }

    @PostMapping("/verifyImageCode")
    public int VerifyCode(String code,HttpServletRequest req){
        String session_vcode=(String) req.getSession().getAttribute("code");
        return session_vcode.equals(code)?1:0;
    }

    @PostMapping("/uploadImg")
    public void uploadImg(HttpServletRequest req,HttpServletResponse resp) throws ServletException, IOException{
        InputStream in = null;
        OutputStream out = null;

        try {
            // 使用默认配置创建解析器工厂
            DiskFileItemFactory factory = new DiskFileItemFactory();
            // 获取解析器
            ServletFileUpload upload = new ServletFileUpload(factory);
            // 上传表单是否为multipart/form-data类型
            if (!upload.isMultipartContent(req)) {
                return;
            }
            // 解析request的输入流
            List<FileItem> fileItemList = upload.parseRequest(req);
            // 迭代list集合
            for (FileItem fileItem : fileItemList) {
                if (fileItem.isFormField()) {
                    // 普通字段
                    String name = fileItem.getFieldName();
                    String value = fileItem.getString();
                    System.out.println(name + "=" + value);
                } else {
                    // 上传文件
                    // 获取上传文件名
                    String realPath=this.getClass().getResource("/").getPath();
                    String savePath=realPath.substring(0,realPath.indexOf("WEB-INF"))+"files";
                    String fileName = fileItem.getName();
                    fileName = UUID.randomUUID().toString().replace("-","")+fileName.substring(fileName.lastIndexOf("\\")+1);
                    // 获取输入流
                    in = fileItem.getInputStream();

                    // 获取上传文件目录
                    // 上传文件名若不存在, 则先创建
                    File savePathDir = new File(savePath);
                    if (!savePathDir.exists()) {
                        savePathDir.mkdir();
                    }

                    // 获取输出流
                    out = new FileOutputStream(savePath + "\\" + fileName);
                    int len = 0;
                    byte[] buffer = new byte[1024];
                    while((len=in.read(buffer)) > 0) {
                        out.write(buffer, 0, len);
                    }
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("url","/files/"+fileName);
                    resp.getWriter().println(jsonObject);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
        }
    }
}
