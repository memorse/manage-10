package com.wanxi.manage.pojo;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 签到表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class Signin implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private User userCode;
    private Date signTime;
    private String signStatus;
    private Date latedTime;
    private Date updateTime;
    private Date createTime;
    private Integer enable;
}
