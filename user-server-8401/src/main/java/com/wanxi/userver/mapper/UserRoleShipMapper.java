package com.wanxi.userver.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.UserRoleShip;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface UserRoleShipMapper extends BaseMapper<UserRoleShip> {
    List<UserRoleShip> getUserRoleShips(@Param("Search") Map<String, Object> map);
    IPage<UserRoleShip> getUserRoleShips(Page page, @Param("Search") Map<String, Object> map);
    int updateShip(UserRoleShip userRoleShip);
    int deleteShip(int id);
    int addUserShip(UserRoleShip userRoleShip);
}
