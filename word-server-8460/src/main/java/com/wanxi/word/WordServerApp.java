package com.wanxi.word;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@MapperScan("com.wanxi.word.mapper")
public class WordServerApp {
    public static void main(String[] args) {
        SpringApplication.run(WordServerApp.class,args);
    }
}
