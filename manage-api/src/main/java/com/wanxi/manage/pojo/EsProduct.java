package com.wanxi.manage.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EsProduct implements Serializable {
   private String username;
   private Integer loginCount;
   private String password;
   private String date;
}
