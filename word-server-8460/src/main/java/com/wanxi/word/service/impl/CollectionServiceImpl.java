package com.wanxi.word.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.WordCollection;
import com.wanxi.word.mapper.CollectionMapper;
import com.wanxi.word.service.CollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Service
public class CollectionServiceImpl  implements CollectionService {

    @Autowired
    private CollectionMapper mapper;
    @Override
    public List<WordCollection> getCollections(Map map) {
        return mapper.getCollections(map);
    }

    @Override
    public int deleteCollection(WordCollection collection) {
        return mapper.deleteCollection(collection);
    }

    @Override
    public int addCollection(WordCollection collection) {
        return mapper.addCollection(collection);
    }

    @Override
    public Integer isExsist(WordCollection collection) {
        return mapper.isExsist(collection);
    }

    @Override
    public IPage<WordCollection> getCollections(Page page, Map map) {
        return mapper.getCollections(page,map);
    }

    @Override
    public int insert(WordCollection entity) {
        return 0;
    }

    @Override
    public int deleteById(Serializable id) {
        return 0;
    }

    @Override
    public int deleteByMap(Map<String, Object> columnMap) {
        return 0;
    }

    @Override
    public int delete(Wrapper<WordCollection> queryWrapper) {
        return 0;
    }

    @Override
    public int deleteBatchIds(java.util.Collection idList) {
        return 0;
    }

    @Override
    public int updateById(WordCollection entity) {
        return 0;
    }

    @Override
    public int update(WordCollection entity, Wrapper<WordCollection> updateWrapper) {
        return 0;
    }

    @Override
    public WordCollection selectById(Serializable id) {
        return null;
    }

    @Override
    public List<WordCollection> selectBatchIds(java.util.Collection idList) {
        return null;
    }

    @Override
    public List<WordCollection> selectByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public WordCollection selectOne(Wrapper<WordCollection> queryWrapper) {
        return null;
    }

    @Override
    public Integer selectCount(Wrapper<WordCollection> queryWrapper) {
        return null;
    }

    @Override
    public List<WordCollection> selectList(Wrapper<WordCollection> queryWrapper) {
        return null;
    }

    @Override
    public List<Map<String, Object>> selectMaps(Wrapper<WordCollection> queryWrapper) {
        return null;
    }

    @Override
    public List<Object> selectObjs(Wrapper<WordCollection> queryWrapper) {
        return null;
    }

    @Override
    public <E extends IPage<WordCollection>> E selectPage(E page, Wrapper<WordCollection> queryWrapper) {
        return null;
    }

    @Override
    public <E extends IPage<Map<String, Object>>> E selectMapsPage(E page, Wrapper<WordCollection> queryWrapper) {
        return null;
    }
}
