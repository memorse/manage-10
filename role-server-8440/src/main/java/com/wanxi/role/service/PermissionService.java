package com.wanxi.role.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.Permission;

import java.util.List;
import java.util.Map;

public interface PermissionService {

    List<Permission> getPermissions(Map<String, Object> map);
    IPage<Permission> getPermissions(Page page, Map<String, Object> map);
    Permission getPermissionByCode(String code);
    Permission getPermissionById(Integer id);
    int deletePermission(Integer id);
    int addPermission(Permission permission);
    int updatePermission(Permission permission);
    List<Permission> getPermissionByUserCode(String userCode);
}
