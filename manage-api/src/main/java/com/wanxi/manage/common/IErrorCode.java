package com.wanxi.manage.common;

public interface IErrorCode {

    long getCode();

    String getMessage();
}
