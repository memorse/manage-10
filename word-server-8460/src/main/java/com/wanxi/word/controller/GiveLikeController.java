package com.wanxi.word.controller;

import com.wanxi.manage.common.CommonResult;
import com.wanxi.manage.pojo.GiveLike;
import com.wanxi.word.service.GiveLikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class GiveLikeController {
    @Autowired
    private GiveLikeService service;

    @GetMapping("/isGiveLike")
    public CommonResult isGiveLike(GiveLike giveLike){
        int exsist = service.isExsist(giveLike);
        return CommonResult.success(exsist);
    }

    @PostMapping("/giveLike")
    public CommonResult addConnection(@RequestBody GiveLike giveLike){
        int i = service.addGiveLike(giveLike);
        return CommonResult.success(i);
    }

    @DeleteMapping("/giveLike")
    public CommonResult delConnection(@RequestBody GiveLike giveLike){
        int i = service.deleteGiveLike(giveLike);
        return CommonResult.success(i);
    }

}
