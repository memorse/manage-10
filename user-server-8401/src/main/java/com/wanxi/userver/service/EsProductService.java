package com.wanxi.userver.service;

import com.wanxi.manage.pojo.EsProduct;

import java.io.IOException;
import java.util.Map;

public interface EsProductService {
    /**
     * 从数据库中导入所有商品到ES
     */
    int importAll() throws IOException;

    /**
     * 根据id删除商品
     */
    boolean delete(String id)throws IOException;

    /**
     * 根据关键字搜索名称或者副标题
     */
    Map search(String keyword) throws IOException;
    int addDocument(EsProduct esProduct)throws IOException;
    int updateCount(Map map, Integer count) throws IOException;
}
