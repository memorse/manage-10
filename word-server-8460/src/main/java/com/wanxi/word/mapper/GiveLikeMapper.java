package com.wanxi.word.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.GiveLike;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GiveLikeMapper extends BaseMapper<GiveLike> {
    List<GiveLike> getGiveLikes(@Param("Search") Map map);
    IPage<GiveLike> getGiveLikes(Page page, @Param("Search") Map map);
    Integer deleteGiveLike(GiveLike giveLike);
    int addGiveLike(GiveLike giveLike);
    int updateGiveLike(GiveLike giveLike);
    Integer isExsist(GiveLike giveLike);
    int getCountByWordCode(String WordCode);

}
