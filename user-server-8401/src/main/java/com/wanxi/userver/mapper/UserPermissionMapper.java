package com.wanxi.userver.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.wanxi.manage.pojo.UserPermission;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface UserPermissionMapper extends BaseMapper<UserPermission> {
    List<UserPermission> getPermissions(@Param("Search") Map<String, Object> map);
    IPage<UserPermission> getPermissions(Page page, @Param("Search") Map<String, Object> map);
    List<UserPermission> getPermissionByPerCode(String code);
    UserPermission getPermissionById(Integer id);
    int deletePermission(Integer id);
    int addPermission(UserPermission permission);
    int updatePermission(UserPermission permission);
    List<UserPermission> getPermissionByUserCode(String userCode);

    int isExsists(@Param("userCode") String userCode, @Param("perCode") String perCode);
}
