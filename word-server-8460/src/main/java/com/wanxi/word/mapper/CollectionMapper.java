package com.wanxi.word.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.WordCollection;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CollectionMapper extends BaseMapper<WordCollection> {
    List<WordCollection> getCollections(@Param("Search") Map map);
    IPage<WordCollection> getCollections(Page<?> page, @Param("Search") Map map);
    Integer deleteCollection(WordCollection collection);
    Integer addCollection(WordCollection collection);
    Integer isExsist(WordCollection collection);
}
