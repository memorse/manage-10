package com.wanxi.daily.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.Permission;
import com.wanxi.manage.pojo.User;
import com.wanxi.manage.vo.LoginParam;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface UserService {

    User getUserByCode(String code);

    List<Permission> getPermissionByUserCode(String userCode);
}