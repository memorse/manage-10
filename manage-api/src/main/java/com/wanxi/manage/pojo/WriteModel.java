package com.wanxi.manage.pojo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WriteModel {

    @ExcelProperty(value="ID",index = 0)
    private Integer id;

    @ExcelProperty(value="单词",index = 1)
    private String english;

    @ExcelProperty(value="中文释义",index = 2)
    private String chinese;

    @ExcelProperty(value="点赞数",index = 3)
    private Integer GiveLikeNum;

    @DateTimeFormat("yyyy-MM-dd")
    @ExcelProperty(value="收藏时间",index = 4)
    private Date createTime;

}
