package com.wanxi.userver.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.UserPermission;
import com.wanxi.userver.mapper.UserPermissionMapper;
import com.wanxi.userver.service.UserPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UserPermissionServiceImpl implements UserPermissionService {
    @Autowired
    private UserPermissionMapper mapper;

    @Override
    public List<UserPermission> getPermissions(Map<String, Object> map) {
        return mapper.getPermissions(map);
    }

    @Override
    public IPage<UserPermission> getPermissions(Page page, Map<String, Object> map) {
        return mapper.getPermissions(page,map);
    }

    @Override
    public List<UserPermission> getPermissionByPerCode(String code) {
        return mapper.getPermissionByPerCode(code);
    }

    @Override
    public UserPermission getPermissionById(Integer id) {
        return mapper.getPermissionById(id);
    }

    @Override
    public int deletePermission(Integer id) {
        return mapper.deletePermission(id);
    }

    @Override
    public int addPermission(List<UserPermission> permission) {
        int result=1;
        for (UserPermission userPermission : permission) {
            int i =mapper.addPermission(userPermission);

            if(i >0)
                result=1;
            else
                result =0;
        }
        return result;
    }

    @Override
    public int updatePermission(List<UserPermission> permission) {
        int result=1;
        for (UserPermission userPermission : permission) {
            int i =mapper.updatePermission(userPermission);

            if(i >0)
                result=1;
            else
                result =0;
        }
        return result;
    }

    @Override
    public int isExsists(String userCode, String perCode) {
        return mapper.isExsists(userCode,perCode);
    }

    @Override
    public List<UserPermission> getPermissionByUserCode(String userCode) {
        return mapper.getPermissionByUserCode(userCode);
    }
}
