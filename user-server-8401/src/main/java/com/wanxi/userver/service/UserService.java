package com.wanxi.userver.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.Permission;
import com.wanxi.manage.pojo.User;
import com.wanxi.manage.vo.LoginParam;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface UserService {
    List<User> getUserBykey(Map<String, Object> map);
    IPage<User> getUserBykey(Page page, Map<String, Object> map);
    List<User> getLeader();
    int addUser(User user);
    int updateUser(User user);
    int deleteUser(Integer id);
    User getUserById(int id);
    User getUserByCode(String code);
    User isLeader(String code);
    List<User> getUserByGroupCode(String code);

    List<Permission> getPermissionByUserCode(String userCode);

    String login(LoginParam loginParams) throws IOException;

    int setEnable(Integer enable, String username);

    int isAvailable(String userCode);
}