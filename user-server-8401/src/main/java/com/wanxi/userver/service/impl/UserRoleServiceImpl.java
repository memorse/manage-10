package com.wanxi.userver.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.UserRoleShip;
import com.wanxi.userver.mapper.UserRoleShipMapper;
import com.wanxi.userver.service.UserRoleShipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UserRoleServiceImpl implements UserRoleShipService {

    @Autowired
    private UserRoleShipMapper mapper;

    @Override
    public List<UserRoleShip> getUserRoleShips(Map<String, Object> map) {
        return mapper.getUserRoleShips(map);
    }

    @Override
    public IPage<UserRoleShip> getUserRoleShips(Page page, Map<String, Object> map) {
        return mapper.getUserRoleShips(page, map);
    }

    @Override
    public int updateShip(UserRoleShip userRoleShip) {
        return mapper.updateShip(userRoleShip);
    }

    @Override
    public int deleteShip(int id) {
        return mapper.deleteShip(id);
    }

    @Override
    public int addUserShip(UserRoleShip userRoleShip) {
        return mapper.addUserShip(userRoleShip);
    }
}
