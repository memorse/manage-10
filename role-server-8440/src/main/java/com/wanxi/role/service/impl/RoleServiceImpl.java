package com.wanxi.role.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.Role;
import com.wanxi.role.mapper.RoleMapper;
import com.wanxi.role.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleMapper roleMapper;
    @Override
    public List<Role> getRoles(Map<String, Object> map) {
        return roleMapper.getRoles(map);
    }

    @Override
    public IPage<Role> getRoles(Page page, Map<String, Object> map) {
        return roleMapper.getRoles(page,map);
    }

    @Override
    public Role getRoleById(int id) {
        return roleMapper.getRoleById(id);
    }

    @Override
    public int updateRole(Role role) {
        return roleMapper.updateRole(role);
    }

    @Override
    public int deleteRole(int id) {
        return roleMapper.deleteRole(id);
    }

    @Override
    public int addRole(Role role) {
        return roleMapper.addRole(role);
    }
}
