package com.wanxi.signin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.Signin;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SigninMapper extends BaseMapper<Signin> {
    List<Signin> getSignins(@Param("Search") Map<String, Object> map);
    IPage<Signin> getSignins(Page page, @Param("Search") Map<String, Object> map);
    Signin getSiginByid(String id);
    int updateSignin(Signin signin);
    int deleteSignin(int id);
    int addSignin(Signin signin);
    int isSignedToday(String date);
}
