package com.wanxi.word.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.EngWord;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface EngWordService {

    List<EngWord> getWords(@Param("Search") Map map);
    IPage<EngWord> getWords(Page page, @Param("Search") Map map);
    int addWord(EngWord engWord);
    int updateWord(EngWord engWord);
    int deleteWord(Integer id);
}
