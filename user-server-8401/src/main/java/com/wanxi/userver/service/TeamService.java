package com.wanxi.userver.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.Team;

import java.util.List;
import java.util.Map;

public interface TeamService {
    List<Team> getTeams(Map<String, Object> map);
    IPage<Team> getTeams(Page page, Map<String, Object> map);
    List<Team> getUsefulTeams();
    Team getTeamById(int id);
    Team getTeamByCode(String code);
    int updateTeam(Team team);
    int deleteTeam(String id);
    int addTeam(Team team);
}
