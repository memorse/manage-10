package com.wanxi.daily.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.DailyReport;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface DailyReportMapper extends BaseMapper<DailyReport> {
    List<DailyReport> getDailyReports(@Param("Search") Map map);
    IPage<DailyReport> getDailyReports(Page page, @Param("Search") Map map);
    DailyReport getDailyReportById(Integer id);
    int deleteReport(Integer id);
    int addReport(DailyReport dailyReport);
    int updateReport(DailyReport dailyReport);

    List<DailyReport> getDailyReportsByUserCode(@Param("Search") Map map);
}
