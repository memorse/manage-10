package com.wanxi.role.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface RoleMapper extends BaseMapper<Role> {
    List<Role> getRoles(@Param("Search") Map<String, Object> map);
    IPage<Role> getRoles(Page page, @Param("Search") Map<String, Object> map);
    Role getRoleById(int id);
    int updateRole(Role role);
    int deleteRole(int id);
    int addRole(Role role);
}
