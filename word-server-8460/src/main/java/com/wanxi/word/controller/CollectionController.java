package com.wanxi.word.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.common.CommonResult;
import com.wanxi.manage.pojo.WordCollection;
import com.wanxi.manage.util.StreamUtil;
import com.wanxi.word.service.CollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class CollectionController {
    @Autowired
    private CollectionService service;

    @GetMapping("/collections")
    public CommonResult getConnections(@RequestParam Map map){
        int currentPage = map.get("currentPage")==null||map.get("currentPage").toString().equals("")?1:Integer.valueOf(map.get("currentPage").toString());
        Integer pageSize = map.get("pageSize")==null||map.get("pageSize").toString().equals("")?10:Integer.valueOf(map.get("pageSize").toString());
        map.put("pageSize",pageSize);
        map.put("key",map.get("key")==null?"":map.get("key"));
        if(map.get("endDate")==null)
            map.put("endDate","");
        if(map.get("startDate")==null)
            map.put("startDate","");
        Page page= new Page(currentPage,pageSize);
        service.getCollections(page,map);
        int pageCount = (int) (page.getTotal()%pageSize==0?page.getTotal()/pageSize:page.getTotal()/pageSize+1);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("list",page.getRecords());
        jsonObject.put("count",pageCount);
        jsonObject.put("currentPage",currentPage);
        jsonObject.put("pageSize",pageSize);
        return  CommonResult.success(jsonObject);
    }


    @PostMapping("/collection")
    public CommonResult addConnection(@RequestBody WordCollection collection){
        int i = service.addCollection(collection);
        return CommonResult.success(i);
    }

    @DeleteMapping("/collection")
    public CommonResult delConnection(@RequestBody WordCollection collection){

        int i = service.deleteCollection(collection);
        return CommonResult.success(i);
    }

    @PostMapping("/isConnection")
    public CommonResult isGiveLike(@RequestBody WordCollection collection){
        int exsist = service.isExsist(collection);
        return CommonResult.success(exsist);
    }

    @PostMapping("/downLoadWordList")
    public CommonResult downLoadWordList(@RequestBody Map map){
        int currentPage = map.get("currentPage")==null||map.get("currentPage").toString().equals("")?1:Integer.valueOf(map.get("currentPage").toString());
        Integer pageSize = map.get("pageSize")==null||map.get("pageSize").toString().equals("")?10:Integer.valueOf(map.get("pageSize").toString());
        int startPage=(currentPage-1)*pageSize;
        map.put("currentPage",startPage);
        map.put("pageSize",pageSize);
        map.put("key",map.get("key")==null?"":map.get("key"));
        if(map.get("endDate")==null)
            map.put("endDate","");
        if(map.get("startDate")==null)
            map.put("startDate","");
        List<WordCollection> collections = service.getCollections(map);
        boolean b = StreamUtil.saveExcelList(collections);
        return CommonResult.success(b?1:0);
    }
}
