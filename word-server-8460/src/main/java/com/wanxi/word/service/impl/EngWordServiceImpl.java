package com.wanxi.word.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.EngWord;
import com.wanxi.word.mapper.EngWordMapper;
import com.wanxi.word.service.EngWordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class EngWordServiceImpl implements EngWordService {

    @Autowired
    private EngWordMapper mapper;
    @Override
    public List<EngWord> getWords(Map map) {
        return mapper.getWords(map);
    }

    @Override
    public IPage<EngWord> getWords(Page page, Map map) {
        return mapper.getWords(page,map);
    }

    @Override
    public int addWord(EngWord engWord) {
        return mapper.addWord(engWord);
    }

    @Override
    public int updateWord(EngWord engWord) {
        return mapper.updateWord(engWord);
    }

    @Override
    public int deleteWord(Integer id) {
        return mapper.deleteWord(id);
    }
}
