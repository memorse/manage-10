package com.wanxi.userver.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.userver.common.JwtTokenUtil;
import com.wanxi.manage.pojo.EsProduct;
import com.wanxi.manage.pojo.MyUserDetails;
import com.wanxi.manage.pojo.Permission;
import com.wanxi.manage.pojo.User;
import com.wanxi.manage.vo.LoginParam;
import com.wanxi.userver.service.RedisService;
import com.wanxi.userver.mapper.UserMapper;
import com.wanxi.userver.service.EsProductService;
import com.wanxi.userver.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.IOException;
import java.net.ConnectException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("userService")
@CacheConfig(cacheNames = "user")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RedisService redisService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @Override
    public List<User> getUserBykey(Map<String, Object> map) {

        List<User> userBykey = userMapper.getUserBykey(map);
        return userBykey;
    }

    @Override
//    @Cacheable(keyGenerator = "keyGenerator")
    public IPage<User> getUserBykey(Page page, Map<String, Object> map) {

        IPage<User> userBykey = userMapper.getUserBykey(page, map);
        if (redisService.hasKey("user::getUserBykey")){
            redisService.del("user::getUserBykey");
        }
        redisService.set("user::getUserBykey",userBykey,60*60*24);
        return userBykey;
    }

    @Override
    public List<User> getLeader() {
        return userMapper.getLeader();
    }

    @Override
    @CacheEvict(key="'getUserBykey'")
    public int addUser(User user) {
        return userMapper.addUser(user);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(key="'getUserBykey'"),
            @CacheEvict(key = "#user.code"),
            @CacheEvict(key = "#user.id")
    })
    public int updateUser(User user) {
        return userMapper.updateUser(user);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(key="'getUserBykey'"),
            @CacheEvict(key = "#id")

    })
    public int deleteUser(Integer id) {
        return userMapper.deleteUser(id);
    }

    @Override
    @Cacheable(key = "#id")
    public User getUserById(int id) {
        redisService.expire("user::"+id,60*60*24);
        return userMapper.getUserById(id);
    }

    @Override
    @Cacheable(key = "#code")
    public User getUserByCode(String code) {
        redisService.expire("user::"+code,60*60*24);
        return userMapper.getUserByCode(code);
    }

    @Override
    public User isLeader(String code) {
        return userMapper.isLeader(code);
    }

    @Override
    public List<User> getUserByGroupCode(String code) {
        return userMapper.getUserByGroupCode(code);
    }

    @Override
    public List<Permission> getPermissionByUserCode(String userCode) {
        return userMapper.getPermissionByUserCode(userCode);
    }

    @Autowired
    private EsProductService esProductService;
    @Override
    public String login(LoginParam loginParams) throws IOException {
        String username = loginParams.getUsername();
        Assert.notNull(username,"账号必须不能为空");
        String password = loginParams.getPassword();
        Assert.notNull(password,"密码必须不能为空");
        User user = userMapper.getUserByCode(username);

        boolean matches = passwordEncoder.matches(password, user.getPassword());
        Map map = null;
        try {
            map = esProductService.search(username);
        } catch (ConnectException e) {
            throw new RuntimeException("ElasticSearch:不存在索引");
        }

        if(matches){

            if(null!=map.get("id") && !"".equals(map.get("id")))
                esProductService.delete(map.get("id").toString());
            return jwtTokenUtil.generateToken(new MyUserDetails(user.getCode(),user.getPassword(),null));
        }else {
            try {


                if (map.size()>0&&null!=map.get("username")){
                    String date = (String) map.get("date");
                    long time = caculateTime(date);
                    Integer loginCount = Integer.valueOf(map.get("loginCount").toString());
                    if(time<30){
                        if(loginCount+1>=3){
                            Assert.isTrue(setEnableForOverThreeTimes(0,username)>0,"输入了三次密码错误，您的帐号已被冻结。");
                        }else {
                            Assert.isTrue(esProductService.updateCount(map,loginCount+1)>0,"帐号或密码错误");
                        }
                    }else {
                        map.put("date",new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()));
                        Assert.isTrue(esProductService.updateCount(map,1)>0,"帐号或密码错误");
                    }

                }else {
                    EsProduct esProduct = new EsProduct();
                    esProduct.setUsername(username);
                    esProduct.setLoginCount(1);
                    esProduct.setDate(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()));
                    Assert.isTrue(esProductService.addDocument(esProduct)>0,"帐号或密码错误");
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return null;

    }



    @Override
//    @Caching(
//            evict = @CacheEvict(key = "#username")
//    )
    public int setEnable(Integer enable,String username){

        int i = userMapper.setEnable(enable,username);
        redisService.del("user::"+username);
       return i;
    }

    public int setEnableForOverThreeTimes(Integer enable,String username){
        int result=1;
        redisService.del("user::"+username);
        if(enable==0){
            User userByCode = getUserByCode(username);
            userByCode.setEnable(0);
            boolean set = redisService.set("user::" + username, userByCode, 60*24);//冻结24小时
            if(set)
                result=1;
            else
                result=0;
        }
        if (enable==1){
            int i = userMapper.setEnable(enable,username);
            if(i>0)
                result=1;
            else
                result=0;
        }
        return result;
    }
    @Override
    public int isAvailable(String userCode){
        int i = userMapper.isAvailable(userCode);
        return i;
    }

    private long caculateTime(String startTiem) throws ParseException {

        Date start = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(startTiem);

        return (new Date().getTime()-start.getTime())/(60*1000);

    }
}
