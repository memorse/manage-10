package com.wanxi.team.service.impl;

import com.wanxi.manage.pojo.Permission;
import com.wanxi.manage.pojo.User;
import com.wanxi.team.mapper.UserMapper;
import com.wanxi.team.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("userService")
@CacheConfig(cacheNames = "user")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;


    @Override
    @Cacheable(key = "#code")
    public User getUserByCode(String code) {
        return userMapper.getUserByCode(code);
    }

    @Override
    public List<Permission> getPermissionByUserCode(String userCode) {
        return userMapper.getPermissionByUserCode(userCode);
    }

}
