package com.wanxi.manage.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdapterCode implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String prjName;
    private String adapterName;
    private String codeKey;
    private String codeValue;
    private Integer orderValue;
    private Date updateTime;
    private Date createTime;
    private Integer enable;
}
