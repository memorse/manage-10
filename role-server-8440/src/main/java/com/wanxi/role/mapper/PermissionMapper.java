package com.wanxi.role.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.Permission;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface PermissionMapper extends BaseMapper<Permission> {
    List<Permission> getPermissions(@Param("Search") Map<String, Object> map);
    IPage<Permission> getPermissions(Page page, @Param("Search") Map<String, Object> map);
    Permission getPermissionByCode(String code);
    Permission getPermissionById(Integer id);
    int deletePermission(Integer id);
    int addPermission(Permission permission);
    int updatePermission(Permission permission);
    List<Permission> getPermissionByUserCode(String userCode);
}
