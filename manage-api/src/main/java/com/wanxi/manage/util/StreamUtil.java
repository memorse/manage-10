package com.wanxi.manage.util;

import com.alibaba.excel.EasyExcel;
import com.wanxi.manage.pojo.WordCollection;
import com.wanxi.manage.pojo.WriteModel;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.Version;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class StreamUtil {

    public static boolean saveWordReport(Map<String,Object> dailyReport){
        try {

            //Configuration 用于读取ftl文件
            Configuration configuration = new Configuration(new Version("2.3.0"));
            configuration.setDefaultEncoding("utf-8");


//            URL wordtemplate = StreamUtil.class.getResource("/static/wordtemplate/");
            /**
             * 指定ftl文件所在目录的路径，而不是ftl文件的路径
             */
            //我的路径是F：/idea_demo/日报.ftl
            configuration.setDirectoryForTemplateLoading(new File("src/main/resources/static/wordtemplate"));
//            configuration.setDirectoryForTemplateLoading(new File("D:\\万息培训\\SpringBoot\\springboot_back\\src\\main\\resources\\static\\wordtemplate\\"));

            String name =new SimpleDateFormat("yyyy-MM-ddHHmmss").format(new Date())+"日报.doc";
            //输出文档路径及名称
            File outFile = new File("D:\\WANXI\\SpringBoot\\springboot_back\\output\\"+name);

            //以utf-8的编码读取ftl文件
            Template template = configuration.getTemplate("model2.ftl", "utf-8");
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), "utf-8"), 10240);
            template.process(dailyReport, out);
            out.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean saveExcelList(List<WordCollection> list){

            try {
                //文件输出位置
                String name ="D:\\WANXI\\SpringBoot\\springboot_back\\output\\"+new SimpleDateFormat("yyyy-MM-ddHHmmss").format(new Date())+".xlsx";
                EasyExcel.write(name, WriteModel.class).sheet("MyConnections").doWrite(createModelList(list));
            }catch (Exception e){
                return false;
            }
        return true;

}

    private static List<WriteModel> createModelList(List<WordCollection> list) {
        List<WriteModel> writeModels=new ArrayList<>();
        for (WordCollection data : list) {
            WriteModel model = new WriteModel();
            model.setId(data.getId());
            model.setChinese(data.getWord().getChinese());
            model.setEnglish(data.getWord().getEnglish());
            model.setGiveLikeNum(data.getWord().getGiveLikeNum());
            model.setCreateTime(data.getCreateTime());
            writeModels.add(model);
        }
        return writeModels;
    }


}
