package com.wanxi.manage.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GiveLike implements Serializable {
    @TableId(type = IdType.AUTO)
    private int id;
    private String engWordCode;
    private String userCode;
    private Date createTime;
}
