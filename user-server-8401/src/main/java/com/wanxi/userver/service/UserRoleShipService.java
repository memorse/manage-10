package com.wanxi.userver.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.UserRoleShip;

import java.util.List;
import java.util.Map;

public interface UserRoleShipService {
    List<UserRoleShip> getUserRoleShips(Map<String, Object> map);
    IPage<UserRoleShip> getUserRoleShips(Page page, Map<String, Object> map);
    int updateShip(UserRoleShip userRoleShip);
    int deleteShip(int id);
    int addUserShip(UserRoleShip userRoleShip);
}
