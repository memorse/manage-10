package com.wanxi.word.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.Permission;
import com.wanxi.manage.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface UserMapper extends BaseMapper<User> {

    List<User> getUserBykey(@Param("Search") Map<String, Object> map);
    IPage<User> getUserBykey(Page page, @Param("Search") Map<String, Object> map);

    int addUser(User user);

    int updateUser(User user);

    int deleteUser(Integer id);

    User getUserById(int id);

    User getUserByCode(String code);

    List<User> getLeader();

    User isLeader(String code);

    List<User> getUserByGroupCode(String code);

    User doLogin(@Param("code") String code, @Param("password") String pwd);

    List<Permission> getPermissionByUserCode(String userCode);

    int setEnable(@Param("enable") Integer enable, @Param("userCode") String username);

    int isAvailable(String userCode);
}