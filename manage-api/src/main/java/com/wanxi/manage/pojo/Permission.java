package com.wanxi.manage.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Permission  implements Serializable  {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String code;
    private String name;
    private String desp;
    private Integer enable;
    @JSONField(format = "yyyy-MM-dd HH:mm")
    private Date updateTime;
    @JSONField(format = "yyyy-MM-dd HH:mm")
    private Date createTime;
}
