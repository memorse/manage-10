package com.wanxi.userver.service.impl;

import com.alibaba.fastjson.JSON;
import com.wanxi.manage.pojo.EsProduct;
import com.wanxi.manage.pojo.User;
import com.wanxi.userver.service.EsProductService;
import com.wanxi.userver.service.UserService;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;


@Service
public class EsProductServiceImpl implements EsProductService {

    @Autowired
    private RestHighLevelClient restHighLevelClient;

    @Autowired
    private UserService userService;
    @Override
    public int importAll() throws IOException {
        HashMap<String, Object> map = new HashMap<>();
        map.put("key","");
        map.put("endDate","");
        map.put("startDate","");
        List<User> all = userService.getUserBykey(map);
        BulkRequest bulkRequest = new BulkRequest();
        for(int i=0;i<all.size();i++){
            EsProduct esProduct = new EsProduct();
            esProduct.setUsername(all.get(i).getCode());
            esProduct.setPassword(all.get(i).getPassword());
            esProduct.setLoginCount(0);
            bulkRequest.add(new IndexRequest("es").source(
                    JSON.toJSONString(esProduct
                    ), XContentType.JSON));
        }

            BulkResponse bulk= restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
            System.out.println(bulk);

        return bulk.status().getStatus();
    }

    @Override
    public boolean delete(String id) throws IOException {
        DeleteRequest es = new DeleteRequest("es");
        es.id(String.valueOf(id));
        DeleteResponse delete = restHighLevelClient.delete(es, RequestOptions.DEFAULT);
        return delete.getShardInfo().getSuccessful()>0;
    }

    @Override
    public Map search(String keyword) throws IOException {
        SearchRequest searchRequest = new SearchRequest("es");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        //精准匹配
        TermQueryBuilder termQuery = QueryBuilders.termQuery("username", keyword);
        searchSourceBuilder.query(termQuery);
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        searchRequest.source(searchSourceBuilder);
        SearchResponse search = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        Map<String, Object> map=new HashMap<>();
        for (SearchHit hit : search.getHits().getHits()) {
            map = hit.getSourceAsMap();
            map.put("id",hit.getId());
        }
        return map;
    }

    @Override
    public int addDocument(EsProduct esProduct) throws IOException {
        IndexRequest indexRequest = new IndexRequest("es");
        indexRequest.source(JSON.toJSONString(esProduct),XContentType.JSON);
        indexRequest.timeout("1s");
        IndexResponse index = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
        int status = index.getShardInfo().getSuccessful();
        return status;

    }

    @Override
    public int updateCount(Map map, Integer count) throws IOException {
        UpdateRequest updateRequest = new UpdateRequest("es",map.get("id").toString());
        map.put("loginCount",count);
        updateRequest.doc(JSON.toJSONString(map),XContentType.JSON);
        UpdateResponse update = restHighLevelClient.update(updateRequest, RequestOptions.DEFAULT);
        return update.getShardInfo().getSuccessful();
    }

}
