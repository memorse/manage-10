package com.wanxi.word.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.GiveLike;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GiveLikeService  {
    List<GiveLike> getGiveLikes(@Param("Search") Map map);
    IPage<GiveLike> getGiveLikes(Page page, @Param("Search") Map map);
    int deleteGiveLike(GiveLike giveLike);
    int addGiveLike(GiveLike giveLike);
    int updateGiveLike(GiveLike giveLike);
    int isExsist(GiveLike giveLike);
    int getCountByWordCode(String WordCode);
}
