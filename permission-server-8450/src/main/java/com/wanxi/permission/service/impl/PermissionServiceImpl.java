package com.wanxi.permission.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.Permission;
import com.wanxi.permission.mapper.PermissionMapper;
import com.wanxi.permission.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private PermissionMapper mapper;

    @Override
    public List<Permission> getPermissions(Map<String, Object> map) {
        return mapper.getPermissions(map);
    }

    @Override
    public IPage<Permission> getPermissions(Page page, Map<String, Object> map) {
        return mapper.getPermissions(page,map);
    }

    @Override
    public Permission getPermissionByCode(String code) {
        return mapper.getPermissionByCode(code);
    }

    @Override
    public Permission getPermissionById(Integer id) {
        return mapper.getPermissionById(id);
    }

    @Override
    public int deletePermission(Integer id) {
        return mapper.deletePermission(id);
    }

    @Override
    public int addPermission(Permission permission) {
        return mapper.addPermission(permission);
    }

    @Override
    public int updatePermission(Permission permission) {
        return mapper.updatePermission(permission);
    }

    @Override
    public List<Permission> getPermissionByUserCode(String userCode) {
        return mapper.getPermissionByUserCode(userCode);
    }
}
