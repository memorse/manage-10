package com.wanxi.userver.controller;

import com.wanxi.manage.common.CommonResult;
import com.wanxi.manage.pojo.UserPermission;
import com.wanxi.userver.service.UserPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class UserPermissionCotrolloer {
    @Autowired
    private UserPermissionService service;

    @PostMapping("/userPermission")
    public CommonResult update(@RequestBody UserPermission userPermission){
        String[] split = userPermission.getPermissionCode().split(",");
        List<UserPermission> list_updata =new ArrayList<>();
        List<UserPermission> list_add =new ArrayList<>();
        for (String s : split) {
            String[] split1 = s.split("::");
            UserPermission per = new UserPermission();
            per.setId(userPermission.getId());
            per.setUserCode(userPermission.getUserCode());
            per.setPermissionCode(split1[0]);
            per.setStatus(Integer.valueOf(split1[1]));
            if(service.isExsists(per.getUserCode(),per.getPermissionCode())>0){
                list_updata.add(per);
            }else {
                list_add.add(per);
            }

        }

        int i = service.updatePermission(list_updata);
        int j = service.addPermission(list_add);
        return CommonResult.success(i>0&&j>0?1:0);
    }
}
