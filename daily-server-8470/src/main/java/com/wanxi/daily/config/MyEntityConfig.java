package com.wanxi.daily.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class MyEntityConfig {

    @Bean
    public RestTemplate myRestTemplate(){
        return new RestTemplate();
    }
//    @Bean
//    public PasswordEncoder passwordEncoder(){
//        return  new BCryptPasswordEncoder();
//    }
}
