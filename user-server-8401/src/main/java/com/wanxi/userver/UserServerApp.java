package com.wanxi.userver;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@MapperScan("com.wanxi.userver.mapper")
public class UserServerApp {
    public static void main(String[] args) {
        SpringApplication.run(UserServerApp.class,args);
    }
}
