package com.wanxi.userver.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.wanxi.manage.common.CommonResult;
import com.wanxi.manage.pojo.UserRoleShip;
import com.wanxi.userver.service.UserRoleShipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@CrossOrigin
@RestController
public class UserRoleShipController {

    @Autowired
    private UserRoleShipService service;

    @GetMapping("/user_role")
    public CommonResult getUsers(String search){

        Map<String,Object> map= JSONObject.parseObject(search,Map.class);

        int currentPage = map.get("currentPage").toString()==null||map.get("currentPage").toString().equals("")?1:Integer.valueOf(map.get("currentPage").toString());
        
        Integer pageSize = map.get("pageSize").toString()==null||map.get("pageSize").toString().equals("")?10:Integer.valueOf(map.get("pageSize").toString());
        
        int startPage=(currentPage-1)*pageSize;
        
        map.put("currentPage",startPage);
        map.put("pageSize",pageSize);
        Page page= new Page(currentPage,pageSize);
        service.getUserRoleShips(page,map);
        int pageCount = (int) (page.getTotal()%pageSize==0?page.getTotal()/pageSize:page.getTotal()/pageSize+1);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("list",page.getRecords());
        jsonObject.put("count",pageCount);
        jsonObject.put("currentPage",currentPage);
        jsonObject.put("pageSize",pageSize);
        return  CommonResult.success(jsonObject);
    }


    @PutMapping("/user_role")
    public String addUser(@RequestBody UserRoleShip userRoleShip){
        int i = service.addUserShip(userRoleShip);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status",200);
        jsonObject.put("result",i);
        return jsonObject.toString();
    }

    @PostMapping("/user_role")
    public String updateUser(@RequestBody UserRoleShip userRoleShip){
        int i = service.updateShip(userRoleShip);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status",200);
        jsonObject.put("result",i);

        return jsonObject.toString();
    }

    @DeleteMapping("/user_role/{id}")
    public String deleteUser(@PathVariable("id") Integer id){
        int i = service.deleteShip(id);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("result",i);
        jsonObject.put("status",200);
        return jsonObject.toString();
    }

}
