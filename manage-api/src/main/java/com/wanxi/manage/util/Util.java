package com.wanxi.manage.util;

import com.alibaba.fastjson.JSONObject;

public class Util {

    public static JSONObject packageJSON(JSONObject jsonObject,int count,int pageSize,int currentPage){
        jsonObject.put("count",count);
        jsonObject.put("currentPage",currentPage);
        jsonObject.put("pageSize",pageSize);
        return jsonObject;
    }

    public static int getPageCount(int total,int pageSize){
        return (total%pageSize==0?total/pageSize:total/pageSize+1);
    }
}
