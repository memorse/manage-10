package com.wanxi.role.service;

import com.wanxi.manage.pojo.Permission;
import com.wanxi.manage.pojo.User;

import java.util.List;

public interface UserService {

    User getUserByCode(String code);

    List<Permission> getPermissionByUserCode(String userCode);
}