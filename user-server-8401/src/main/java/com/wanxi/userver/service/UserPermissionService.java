package com.wanxi.userver.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.UserPermission;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface UserPermissionService {
    List<UserPermission> getPermissions(@Param("Search") Map<String, Object> map);
    IPage<UserPermission> getPermissions(Page page, @Param("Search") Map<String, Object> map);
    List<UserPermission> getPermissionByPerCode(String code);
    UserPermission getPermissionById(Integer id);
    int deletePermission(Integer id);
    int addPermission(List<UserPermission> permission);
    int updatePermission(List<UserPermission> permission);
    int isExsists(String userCode, String perCode);
    List<UserPermission> getPermissionByUserCode(String userCode);
}
