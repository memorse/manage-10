package com.wanxi.daily.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.DailyReport;

import java.util.List;
import java.util.Map;

public interface DailyReportService {
    List<DailyReport> getDailyReports(Map map);
    IPage<DailyReport> getDailyReports(Page page, Map map);
    DailyReport getDailyReportById(Integer id);
    int deleteReport(Integer id);
    int addReport(DailyReport dailyReport);
    int updateReport(DailyReport dailyReport);

    List<DailyReport> getDailyReportsByUserCode(Map map);
}
