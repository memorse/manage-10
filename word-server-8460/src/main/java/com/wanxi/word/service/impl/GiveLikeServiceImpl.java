package com.wanxi.word.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.GiveLike;
import com.wanxi.word.mapper.GiveLikeMapper;
import com.wanxi.word.service.GiveLikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class GiveLikeServiceImpl implements GiveLikeService {
    @Autowired
    private GiveLikeMapper mapper;
    @Override
    public List<GiveLike> getGiveLikes(Map map) {
        return mapper.getGiveLikes(map);
    }

    @Override
    public IPage<GiveLike> getGiveLikes(Page page, Map map) {
        return mapper.getGiveLikes(page,map);
    }

    @Override
    public int deleteGiveLike(GiveLike giveLike) {
        return mapper.deleteGiveLike(giveLike);
    }

    @Override
    public int addGiveLike(GiveLike giveLike) {
        return mapper.addGiveLike(giveLike);
    }

    @Override
    public int updateGiveLike(GiveLike giveLike) {
        return mapper.updateGiveLike(giveLike);
    }

    @Override
    public int isExsist(GiveLike giveLike) {
        return mapper.isExsist(giveLike);
    }

    @Override
    public int getCountByWordCode(String WordCode) {
        return mapper.getCountByWordCode(WordCode);
    }
}
