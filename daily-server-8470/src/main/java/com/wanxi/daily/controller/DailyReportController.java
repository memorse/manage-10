package com.wanxi.daily.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.daily.service.DailyReportService;
import com.wanxi.manage.common.CommonResult;
import com.wanxi.manage.pojo.DailyReport;
import com.wanxi.manage.util.StreamUtil;
import com.wanxi.manage.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class DailyReportController {

    @Autowired
    private DailyReportService service;

    @GetMapping("/dailyReports")
    public CommonResult getDailyReports(@RequestParam Map map){
        int currentPage = map.get("currentPage")==null||map.get("currentPage").toString().equals("")?1:Integer.valueOf(map.get("currentPage").toString());
        Integer pageSize = map.get("pageSize")==null||map.get("pageSize").toString().equals("")?10:Integer.valueOf(map.get("pageSize").toString());
        int startPage=(currentPage-1)*pageSize;
        map.put("currentPage",startPage);
        map.put("pageSize",pageSize);
        map.put("key",map.get("key")==null?"":map.get("key"));
        if(map.get("endDate")==null)
            map.put("endDate","");
        if(map.get("startDate")==null)
            map.put("startDate","");
        Page page = new Page(currentPage, pageSize);
        service.getDailyReports(page,map);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("list",page.getRecords());
        int pageCount = Util.getPageCount((int) page.getTotal(), pageSize);
        JSONObject jsonObject1 = Util.packageJSON(jsonObject, pageCount, pageSize, currentPage);
        return CommonResult.success(jsonObject1);
    }

    @GetMapping("/userDailyReports")
    public CommonResult getDailyReportsByUserCode(@RequestParam Map map){

        List<DailyReport> dailyReports = service.getDailyReportsByUserCode(map);
        return CommonResult.success(dailyReports);
    }

    @PreAuthorize("hasAnyAuthority('admin')")
    @GetMapping("/dailyReport/{id}")
    public CommonResult getReportById(@PathVariable("id") Integer id){
        DailyReport dailyReportById = service.getDailyReportById(id);
        return CommonResult.success(dailyReportById);
    }

    @PostMapping("/dailyReport")
    public CommonResult update(@RequestBody DailyReport dailyReport){
        int i = service.updateReport(dailyReport);
        return CommonResult.success(i);
    }

    @PreAuthorize("hasAuthority('alterReportToViewed')")
    @PostMapping("/updateReportStatus")
    public CommonResult updateReportStatus(@RequestBody DailyReport dailyReport){
        dailyReport.setEnable(2);
        int i = service.updateReport(dailyReport);
        return CommonResult.success(i);
    }

    @PutMapping("/dailyReport")
    public CommonResult add(@RequestBody DailyReport dailyReport){
        int i = service.addReport(dailyReport);
        return CommonResult.success(i);
    }

    @DeleteMapping("/dailyReport/{id}")
    public CommonResult delete(@PathVariable("id") Integer id){
        int i = service.deleteReport(id);
        return CommonResult.success(i);
    }

    @PreAuthorize("hasAuthority('download_word')")
    @PostMapping("/downloadReport/{id}")
    public CommonResult downloadReport(@PathVariable("id") Integer id){
        DailyReport report = service.getDailyReportById(id);
        Map<String,Object> map=new HashMap<>();
        map.put("name",report.getName());
        map.put("coatch",report.getCoatch());
        map.put("teacher",report.getTeacher());
        map.put("degree",report.getDegree());
        map.put("gain",report.getGain());
        map.put("deficiency",report.getDeficiency());
        map.put("datetime",new SimpleDateFormat("yyyy-MM-dd").format(report.getDateTime()));
        boolean b = StreamUtil.saveWordReport(map);
        if (b)
            return CommonResult.success("已下载");
        else
            return CommonResult.failed("请稍后重试！");
    }
}
