package com.wanxi.userver.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.common.CommonResult;
import com.wanxi.manage.pojo.Team;
import com.wanxi.manage.pojo.User;
import com.wanxi.manage.util.Util;
import com.wanxi.manage.vo.LoginParam;
import com.wanxi.userver.service.TeamService;
import com.wanxi.userver.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class UserController {

//    @Value("${jwt.tokenHead}")
    String tokenHead="bearer";
    @Autowired
    private UserService userService;

//    @Value("${service.url.team}")
    @Autowired
    private TeamService teamService;

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @PreAuthorize("hasAuthority('query_all')")
    @GetMapping("/user")
    public CommonResult getUsers(@RequestParam Map map){
        int currentPage = map.get("currentPage")==null||map.get("currentPage").toString().equals("")?1:Integer.valueOf(map.get("currentPage").toString());
        Integer pageSize = map.get("pageSize")==null||map.get("pageSize").toString().equals("")?10:Integer.valueOf(map.get("pageSize").toString());
        int startPage=(currentPage-1)*pageSize;
        map.put("currentPage",startPage);
        map.put("pageSize",pageSize);
        if(map.get("endDate")==null)
            map.put("endDate","");
        if(map.get("startDate")==null)
            map.put("startDate","");
        String userId1 = (String) map.get("userId");
        if(null!=userId1 &&!"".equals(userId1)){
            User userId = userService.getUserByCode(userId1);
            map.put("team",userId.getGroupCode().getTeamCode());
        }
        HashMap<String, Object> teamMap = new HashMap<>();
        teamMap.put("startDate","");
        teamMap.put("endDate","");
        List<Team> teams = teamService.getTeams(teamMap);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("teams",teams);
        Page page = new Page(currentPage, pageSize);
        IPage userBykey = userService.getUserBykey(page, map);
        jsonObject.put("users",userBykey.getRecords());
        int pageCount = Util.getPageCount((int) page.getTotal(), pageSize);
        JSONObject jsonObject1 = Util.packageJSON(jsonObject, pageCount, pageSize, currentPage);
        return CommonResult.success(jsonObject1);
    }

    @PostMapping("/login")
    public CommonResult login(@RequestBody LoginParam loginParams){

//        int available = userService.isAvailable(loginParams.getUsername());
        User userByCode = userService.getUserByCode(loginParams.getUsername());
        if (userByCode.getEnable()==0){
            return CommonResult.failed("您的帐号已被冻结");
        }
        HashMap<String, String> data = new HashMap<>();
        String token = null;
        try {
            token = userService.login(loginParams);
        }catch (Exception e) {
            e.printStackTrace();
            return CommonResult.validateFailed(e.getMessage());
        }
        if (StringUtils.isEmpty(token)){
            return CommonResult.validateFailed("用户名或密码错误");
        }
        data.put("tokenHead",tokenHead);
        data.put("access_token",token);
        // localStorage.setItem("Authorization","Bearer sdsdfdfds")
        // $ajax{data:{},type:"",header:{"Authorization":"Bearer sdsdfdfds"}}
        return CommonResult.success(data);
    }
//=============================

        @GetMapping("/isLeader")
        public CommonResult isLeader(String code){
            User leader = userService.isLeader(code);
            JSONObject jsonObject = new JSONObject();
            if (leader!=null){
                jsonObject.put("status",200);
                jsonObject.put("result",1);
            }else {
                jsonObject.put("status",200);
                jsonObject.put("result",0);
            }
            return CommonResult.success(jsonObject);
        }

        @GetMapping("/user/{id}")
        public CommonResult getUserById(@PathVariable("id") int id){
            return CommonResult.success(userService.getUserById(id));
        }

        @GetMapping("/userByCode/{code}")
        public CommonResult getUserByCode(@PathVariable("code") String code){
            return CommonResult.success(userService.getUserByCode(code));
        }

        @GetMapping("/getUserByTeam")
        public CommonResult getUserByTeam(String code){
            List<User> leaders = userService.getUserByGroupCode(code);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("leaders",leaders);
            return  CommonResult.success(jsonObject);
        }

        @PutMapping("/user")
        public CommonResult addUser(@RequestBody User user){
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            int i = userService.addUser(user);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status",200);
            jsonObject.put("result",i);
            return  CommonResult.success(jsonObject);
        }

        @PostMapping("/user")
        public CommonResult updateUser(@RequestBody User user){
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            int i = userService.updateUser(user);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status",200);
            jsonObject.put("result",i);

            return  CommonResult.success(jsonObject);
        }
        @PreAuthorize("hasAuthority('delete_user')")
        @DeleteMapping("/user/{id}")
        public CommonResult<JSONObject> deleteUser(@PathVariable("id") Integer id){
            int i = userService.deleteUser(id);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("result",i);
            jsonObject.put("status",200);
            return  CommonResult.success(jsonObject);
        }

        @PostMapping("/user/lock")
        public CommonResult lockUser(@RequestBody Map map){
        try {
            String userCode = (String) map.get("userName");
            Integer enable= (Integer) map.get("enable");
            int i = userService.setEnable(enable, userCode);
            return CommonResult.success(i);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

        @GetMapping("/user/isAvailable")
        public CommonResult isAvailable(@RequestParam String userCode){
            int available = userService.isAvailable(userCode);
            return CommonResult.success(available);
        }
}
