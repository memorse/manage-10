package com.wanxi.role.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.wanxi.manage.common.CommonResult;
import com.wanxi.manage.pojo.RolePermissionRalation;
import com.wanxi.manage.util.Util;
import com.wanxi.role.service.PermissionService;
import com.wanxi.role.service.RolePermissionRalationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class RolePermissionRelationController {
    @Autowired
    private RolePermissionRalationService service;

    @Autowired
    private PermissionService permissionService;

    @GetMapping("/role_permission")
//    @PreAuthorize("hasAnyAuthority('query_all','query_role_permission')")
    public CommonResult getRolePermission(@RequestParam Map map){
        int currentPage = map.get("currentPage")==null||map.get("currentPage").toString().equals("")?1:Integer.valueOf(map.get("currentPage").toString());
        Integer pageSize = map.get("pageSize")==null||map.get("pageSize").toString().equals("")?10:Integer.valueOf(map.get("pageSize").toString());
        int startPage=(currentPage-1)*pageSize;
        map.put("currentPage",startPage);
        map.put("pageSize",pageSize);
        map.put("key",map.get("key")==null?"":map.get("key"));
        if(map.get("endDate")==null)
            map.put("endDate","");
        if(map.get("startDate")==null)
            map.put("startDate","");
        Page page = new Page(currentPage, pageSize);
        service.getPermissions(page,map);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("list",page.getRecords());
        int pageCount = Util.getPageCount((int) page.getTotal(), pageSize);
        JSONObject jsonObject1 = Util.packageJSON(jsonObject, pageCount, pageSize, currentPage);
        return CommonResult.success(jsonObject1);
    }

    @GetMapping("/role_permission/{roleCode}")
    public CommonResult getPermissionByRoleCode(@PathVariable("roleCode") String roleCode){
        List<RolePermissionRalation> rolePermissionByRoleCode = service.getRolePermissionByRoleCode(roleCode);
        return CommonResult.success(rolePermissionByRoleCode);
    }

    @PostMapping("/role_permission")
    public CommonResult updataPermission(@RequestBody RolePermissionRalation rolePermissionRalation){
        String[] split = rolePermissionRalation.getPermissions().split(",");
        List<RolePermissionRalation> list_updata =new ArrayList<>();
        List<RolePermissionRalation> list_add =new ArrayList<>();
        for (String s : split) {
            String[] split1 = s.split("::");
            RolePermissionRalation per = new RolePermissionRalation();
            per.setId(rolePermissionRalation.getId());
            per.setRoleCode(rolePermissionRalation.getRoleCode());
            per.setRoleName(rolePermissionRalation.getRoleName());
            per.setPermissions(split1[0]);
            per.setEnable(Integer.valueOf(split1[1]));
            if(service.isExsists(per.getRoleCode(),per.getPermissions())>0){
                list_updata.add(per);
            }else {
                if(per.getEnable()==1)
                    list_add.add(per);
            }

        }

        int i = service.updatePermission(list_updata);
        int j = service.addPermission(list_add);
        return CommonResult.success(i>0&&j>0?1:0);
    }



}
