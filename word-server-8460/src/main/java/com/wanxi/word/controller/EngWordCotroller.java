package com.wanxi.word.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.common.CommonResult;
import com.wanxi.manage.pojo.EngWord;
import com.wanxi.manage.util.Util;
import com.wanxi.word.service.EngWordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.UUID;

@RestController
public class EngWordCotroller {
    @Autowired
    private EngWordService service;

    @GetMapping("/engWrods")
    public CommonResult getWrods(@RequestParam Map map){
        map.put("key",null==map.get("key")?"":map.get("key"));
        int currentPage = map.get("currentPage")==null||map.get("currentPage").toString().equals("")?1:Integer.valueOf(map.get("currentPage").toString());
        Integer pageSize = map.get("pageSize")==null||map.get("pageSize").toString().equals("")?10:Integer.valueOf(map.get("pageSize").toString());
        int startPage=(currentPage-1)*pageSize;
        map.put("currentPage",startPage);
        map.put("pageSize",pageSize);
        map.put("key",map.get("key")==null?"":map.get("key"));
        if(map.get("endDate")==null)
            map.put("endDate","");
        if(map.get("startDate")==null)
            map.put("startDate","");
        Page page = new Page(currentPage, pageSize);
        service.getWords(page,map);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("list",page.getRecords());
        int pageCount = Util.getPageCount((int) page.getTotal(), pageSize);
        JSONObject jsonObject1 = Util.packageJSON(jsonObject, pageCount, pageSize, currentPage);
        return CommonResult.success(jsonObject1);
    }

    @PostMapping("/engWord")
    public CommonResult updateWord(@RequestBody EngWord engWord){
        int i = service.updateWord(engWord);
        return CommonResult.success(i);
    }
    @PutMapping("/engWord")
    public CommonResult addWord(@RequestBody EngWord engWord){
        engWord.setCode(UUID.randomUUID().toString().replace("-",""));
        int i = service.addWord(engWord);
        return CommonResult.success(i);
    }

    @DeleteMapping("/engWord/{id}")
    public CommonResult delWord(@PathVariable("id") Integer id){
        int i = service.deleteWord(id);
        return CommonResult.success(i);
    }
}
