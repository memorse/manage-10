package com.wanxi.daily.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.daily.mapper.DailyReportMapper;
import com.wanxi.daily.service.DailyReportService;
import com.wanxi.manage.pojo.DailyReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class DailyReportServiceImpl implements DailyReportService {
    @Autowired
    private DailyReportMapper mapper;
    @Override
    public List<DailyReport> getDailyReports(Map map) {
        return mapper.getDailyReports(map);
    }

    @Override
    public IPage<DailyReport> getDailyReports(Page page, Map map) {
        return mapper.getDailyReports(page,map);
    }

    @Override
    public DailyReport getDailyReportById(Integer id) {
        return mapper.getDailyReportById(id);
    }

    @Override
    public int deleteReport(Integer id) {
        return mapper.deleteReport(id);
    }

    @Override
    public int addReport(DailyReport dailyReport) {
        return mapper.addReport(dailyReport);
    }

    @Override
    public int updateReport(DailyReport dailyReport) {
        return mapper.updateReport(dailyReport);
    }

    @Override
    public List<DailyReport> getDailyReportsByUserCode(Map map) {
        return mapper.getDailyReportsByUserCode(map);
    }
}
