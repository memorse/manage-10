package com.wanxi.team.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.common.CommonResult;
import com.wanxi.manage.pojo.Team;
import com.wanxi.manage.util.Util;
import com.wanxi.team.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@CrossOrigin
@RestController
public class TeamController {
    
    @Autowired
    private TeamService teamService;

    @Value("${service.url.user}")
    private String userService;

    @Autowired
    private RestTemplate restTemplate;
    @GetMapping("/team")
    public CommonResult getUsers(String search){

        Map<String,Object> map= JSONObject.parseObject(search,Map.class);

        int currentPage = map.get("currentPage").toString()==null||map.get("currentPage").toString().equals("")?1:Integer.valueOf(map.get("currentPage").toString());
        Integer pageSize = map.get("pageSize").toString()==null||map.get("pageSize").toString().equals("")?10:Integer.valueOf(map.get("pageSize").toString());
        int startPage=(currentPage-1)*pageSize;
        map.put("currentPage",startPage);
        map.put("pageSize",pageSize);
        Page page = new Page(currentPage, pageSize);
        teamService.getTeams(page,map);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("list",page.getRecords());
        int pageCount = Util.getPageCount((int) page.getTotal(), pageSize);
        JSONObject jsonObject1 = Util.packageJSON(jsonObject, pageCount, pageSize, currentPage);
        return CommonResult.success(jsonObject1);
    }

    @GetMapping("/team/{id}")
    public CommonResult getUserById(@PathVariable("id") int id){
        Team teamById = teamService.getTeamById(id);
//        int count = userMapper.getUserByGroupCode(teamById.getTeamCode()).size();
//        teamById.setTeamCrew(count);
        return CommonResult.success(teamById);
    }

    @PutMapping("/team")
    public CommonResult addUser(@RequestBody Team team){
        int i = teamService.addTeam(team);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status",200);
        jsonObject.put("result",i);
        return CommonResult.success(jsonObject);
    }

    @PostMapping("/team")
    public CommonResult updateUser(@RequestBody Team team){
        int i = teamService.updateTeam(team);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status",200);
        jsonObject.put("result",i);
        return CommonResult.success(jsonObject);
    }

    @DeleteMapping("/team/{code}")
    public CommonResult deleteUser(@PathVariable("code") String code){
//        List<User> users = userMapper.getUserByGroupCode(code);
        JSONObject jsonObject = new JSONObject();
//        if(users.size()>0)
//        {
//            jsonObject.put("result",0);
//            jsonObject.put("status",200);
//        }else {
//            int i = teamService.deleteTeam(code);
//
//            jsonObject.put("result",i);
//            jsonObject.put("status",200);
//        }

        return CommonResult.success(jsonObject);
    }

}
