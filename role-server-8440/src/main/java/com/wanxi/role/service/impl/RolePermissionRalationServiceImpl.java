package com.wanxi.role.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.RolePermissionRalation;
import com.wanxi.role.mapper.RolePermissionRelationMapper;
import com.wanxi.role.service.RolePermissionRalationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class RolePermissionRalationServiceImpl implements RolePermissionRalationService {

    @Autowired
    private RolePermissionRelationMapper mapper;

    @Override
    public List<RolePermissionRalation> getPermissions(Map<String, Object> map) {
        return mapper.getPermissions(map);
    }

    @Override
    public IPage<RolePermissionRalation> getPermissions(Page page, Map<String, Object> map) {
        return mapper.getPermissions(page,map);
    }

    @Override
    public List<RolePermissionRalation> getRolePermissionByRoleCode(String roleCode) {
        return mapper.getRolePermissionByRoleCode(roleCode);
    }

    @Override
    public List<RolePermissionRalation> getRolePermissionByPerCode(String perCode) {
        return mapper.getRolePermissionByPerCode(perCode);
    }

    @Override
    public RolePermissionRalation getRolePermissionById(Integer id) {
        return mapper.getRolePermissionById(id);
    }

    @Override
    public int deletePermission(Integer id) {
        return mapper.deletePermission(id);
    }

    @Override
    public int addPermission(List<RolePermissionRalation> permissions) {
        int result=1;
        for (RolePermissionRalation permission : permissions) {

            int i=mapper.addPermission(permission);
            if (i>0)
                result=1;
            else
                result =0;
        }
        return result;
    }

    @Override
    public int updatePermission(List<RolePermissionRalation> permissions) {
        int result=1;
        for (RolePermissionRalation permission : permissions) {

            int i=mapper.updatePermission(permission);
            if (i>0)
                result=1;
            else
                result =0;
        }
        return result;
    }

    @Override
    public int isExsists(String roleCode, String perCode) {
        return mapper.isExsists(roleCode,perCode);
    }
}
