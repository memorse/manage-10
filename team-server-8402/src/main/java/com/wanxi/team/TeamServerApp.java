package com.wanxi.team;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@MapperScan("com.wanxi.team.mapper")
public class TeamServerApp {
    public static void main(String[] args) {
        SpringApplication.run(TeamServerApp.class,args);
    }
}
