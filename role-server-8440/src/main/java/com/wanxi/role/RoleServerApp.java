package com.wanxi.role;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@MapperScan("com.wanxi.role.mapper")
public class RoleServerApp {
    public static void main(String[] args) {
        SpringApplication.run(RoleServerApp.class,args);
    }
}
