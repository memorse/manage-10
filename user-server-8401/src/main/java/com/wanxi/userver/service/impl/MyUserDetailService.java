package com.wanxi.userver.service.impl;

import com.wanxi.manage.pojo.MyUserDetails;
import com.wanxi.manage.pojo.Permission;
import com.wanxi.manage.pojo.User;
import com.wanxi.userver.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service("userDetailsService")
public class MyUserDetailService implements UserDetailsService {
    @Autowired
    UserService userService;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.getUserByCode(username);

        List<Permission> permissionList = userService.getPermissionByUserCode(user.getCode());
        HashSet<Permission> permissions = new HashSet<>(permissionList);
        List<SimpleGrantedAuthority> authorities = permissions.stream().filter(permission -> permission.getCode() != null)
                .map(permission -> new SimpleGrantedAuthority(permission.getCode()))
                .collect(Collectors.toList());
        MyUserDetails myUserDetails = new MyUserDetails(user.getCode(),user.getPassword(),authorities);
        return myUserDetails;
    }
}
