package com.wanxi.manage.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.util.Date;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserPermission implements Serializable {
    @TableId(type = IdType.AUTO)
    private int id;
    private String userCode;
    private String permissionCode;
    private Integer status;
    private Date updateTime;
    private Date createTime;

}
