package com.wanxi.role.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanxi.manage.pojo.Role;

import java.util.List;
import java.util.Map;

public interface RoleService {
    List<Role> getRoles(Map<String, Object> map);
    IPage<Role> getRoles(Page page, Map<String, Object> map);
    Role getRoleById(int id);
    int updateRole(Role role);
    int deleteRole(int id);
    int addRole(Role role);
}
